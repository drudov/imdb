import React from 'react'

import { Field, reduxForm } from 'redux-form'
import InputField from 'components/searchForm/InputField'


export const SearchFormName = 'MovieSearchForm'
export const MOVIE_FIELD = 'movie'

const SearchForm = ({ onSearchChange }) => {
  return (
    <div className="movie-search-form">
      <Field name={MOVIE_FIELD}
             component={InputField}
             onChange={onSearchChange && onSearchChange}
      />
    </div>
  )
}

export default reduxForm({
  form: SearchFormName,
  enableReinitialize: true
})(SearchForm)
