import React from 'react'
import { Input } from 'antd'


const InputField = ({ input: { value, onChange } }) => {

  return <Input value={value}
                onChange={onChange}
  />
}

export default InputField
