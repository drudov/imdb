import React, { Component } from 'react'
import PropTypes from 'prop-types'
import deb from 'lodash/debounce'
import { connect } from 'react-redux'
import qs from 'query-string'
import getPopular from 'store/actions/getPopular'
import { GetCurrentPage, GetMoviesSelector, GetTotalPages, GetTotalResults } from 'helpers/selectors'
import MoviesList from 'components/moviesList/MoviesList'
import SearchForm from 'components/searchForm/SearchForm'
import './moviesListContainer.scss'
import routes from 'helpers/url'
import { BackTop } from 'antd'


const PAGE_SIZE = 20

class MoviesListContainer extends Component {

  static propTypes = {
    getMovies: PropTypes.func.isRequired,
    movies: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        'original_title': PropTypes.string.isRequired,
      }),
    ),
    totalResults: PropTypes.number.isRequired,
    currentPage: PropTypes.number,
  }

  componentDidMount() {
    this.__getMovies()
  }

  componentDidUpdate(prevProps) {
    const { location: { search } } = this.props
    const { location: { search: oldSearch } } = prevProps

    if (search !== oldSearch) {
      this.__getMovies()
    }
  }

  /**
   * Get Movies action
   * @returns {boolean}
   * @private
   */
  __getMovies = () => {
    const { location: { search }, getMovies } = this.props
    if (!getMovies) {
      return false
    }

    const { page } = qs.parse(search)
    getMovies(page || 1)
  }

  /**
   * Search Handler with debounce
   */
  __onSearchMovie = deb(({ target: { value: movie } }) => {
    const { history: { push } } = this.props
    push(`?${qs.stringify({ movie })}`)

  }, 1500)

  /**
   * Handle page changing
   * @param page
   * @private
   */
  __onPageChange = (page) => {
    const { history: { push }, location: { search } } = this.props
    const ls = qs.stringify({ ...qs.parse(search), page })
    push(`?${ls}`)
  }

  __onItemClick = (id) => {
    const { history: { push } } = this.props

    push(routes.movieDetails(id))
  }

  render() {
    const { movies, loading, totalResults, currentPage, location: { search } } = this.props

    return (
      <div className="movies-list">
        <div className="movies-list__header">
          <SearchForm
            onSearchChange={this.__onSearchMovie}
            initialValues={qs.parse(search)}
          />
        </div>
        <MoviesList
          loading={loading}
          onPageChange={deb(this.__onPageChange, 500)}
          movies={movies}
          currentPage={currentPage}
          pageSize={PAGE_SIZE}
          totalResults={totalResults}
          onItemClick={this.__onItemClick}
        />
        <BackTop visibilityHeight={100}/>
      </div>
    )
  }
}

export default connect(({ main: { loading, ...main } }) => ({
  movies: GetMoviesSelector(main) || [],
  totalPages: GetTotalPages(main),
  totalResults: GetTotalResults(main) || 0,
  currentPage: GetCurrentPage(main) || 1,
  loading,
}), {
  getMovies: getPopular,
})(MoviesListContainer)
