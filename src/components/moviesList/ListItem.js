import React from 'react'
import { Avatar, List } from 'antd'
import PropTypes from 'prop-types'


const { Item } = List

const ListItem = ({ id, poster_path, title, overview, genres, original_title, onClick }) => {
  return (
    <Item
      onClick={() => onClick && onClick(id)}
      key={id}
      className="list-item"
      extra={<img src={`http://image.tmdb.org/t/p/w185/${poster_path}`}
                  alt={original_title}
      />}
    >
      <Item.Meta avatar={<Avatar src={`http://image.tmdb.org/t/p/w185/${poster_path}`}/>}
                 title={title}
                 description={genres.map(({ name }) => name).join(', ')}
      />
      {overview}
    </Item>
  )
}


ListItem.propTypes = {
  id: PropTypes.number.isRequired,
}

export default ListItem
