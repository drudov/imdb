import React from 'react'
import PropTypes from 'prop-types'
import { List } from 'antd'
import ListItem from 'components/moviesList/ListItem'

/**
 * List of movies
 * @param movies
 * @param loading
 * @param pageSize
 * @param totalResults
 * @param onPageChange
 * @param currentPage
 * @param onItemClick
 * @returns {*}
 * @constructor
 */
const MoviesList = ({ movies, loading, pageSize, totalResults, onPageChange, currentPage, onItemClick }) => {
  return <List
    dataSource={movies}
    renderItem={item => <ListItem {...item} onClick={onItemClick}/>}
    loading={loading}
    itemLayout="vertical"
    pagination={{
      pageSize: pageSize,
      total: totalResults,
      onChange: onPageChange,
      current: currentPage
    }}
  />
}


MoviesList.propTypes = {
  movies: PropTypes.array,
  loading: PropTypes.bool,
  pageSize: PropTypes.number,
  totalResults: PropTypes.number,
  onPageChange: PropTypes.func.isRequired,
  onItemClick: PropTypes.func.isRequired,
  currentPage: PropTypes.number.isRequired
}

export default MoviesList
