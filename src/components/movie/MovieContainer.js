import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Spin } from 'antd'
import Movie from 'components/movie/Movie'
import getMovieDetails from 'store/actions/getMovieDetails'

class MovieContainer extends Component {


  componentDidMount() {
    this.getMovie()
  }


  getMovie = () => {
    const { match: { params: { movieID } }, getMovieDetails } = this.props
    getMovieDetails(movieID)
  }


  render() {
    const { movie, loading } = this.props


    if (loading) {
      return <Spin spinning
                   className={'loading-spinner'}
                   size={'large'}
      />
    }

    return <Movie {...movie}/>
  }
}

export default connect(({ main: { movieDetails, loading } }) => ({
  movie: movieDetails,
  loading: loading || !movieDetails,
}), { getMovieDetails })(MovieContainer)