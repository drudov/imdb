import React from 'react'
import './movie.scss'


const Movie = (props) => {
  const {
    original_title,
    overview,
    poster_path,
    vote_average
  } = props

  return (
    <div className="movie">
      <h2>{original_title}</h2>

      <div className="movie__description">
        <div className="img">
          <img

            src={`http://image.tmdb.org/t/p/w185/${poster_path}`}
            alt={original_title}
          />
        </div>
        <div className="text">
          {overview}
        </div>
      </div>

      <div className="movie__vote">
        {vote_average}
      </div>

    </div>
  )
}

export default Movie
