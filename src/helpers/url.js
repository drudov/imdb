const routes = {
  moviePageRoute: '/',
  movieDetails: (id) => `/${id || ':movieID'}`,
}


export default routes