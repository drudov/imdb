import axios from 'axios'
import jsonpAdapter from 'axios-jsonp'
import { API_BASE, API_KEY } from 'helpers/constants'

const DEFAULT_CALLBACK = 'callback'


/**
 * JsonP Get Helper
 * @param endpoint
 * @param params
 * @returns {Promise<boolean|any>}
 * @constructor
 */
export const Get = async (endpoint, params = {}) => {

  const res = await axios(`${API_BASE}${endpoint}`, {
    adapter: jsonpAdapter,
    callbackParamName: DEFAULT_CALLBACK,
    params: {
      api_key: API_KEY,
      ...params,
    },
  })

  const { status, data } = res

  return status === 200 && data
}


/**
 * Get Popular Movies
 * @type {function(Promise<*>=): Promise<boolean|any>}
 */
export const GetPopular = (page = 1) => Get('/movie/popular', { page })

/**
 * Get Genres
 * @returns {Promise<boolean|any>}
 * @constructor
 */
export const GetGenres = () => Get('/genre/movie/list')

/**
 * Search Movie
 * @param query
 * @param page
 * @returns {Promise<boolean|any>}
 * @constructor
 */
export const SearchMovie = (query, page) => Get('/search/movie', { query, page })

/**
 * Get Movie Details
 * @param id
 * @returns {Promise<boolean|any>}
 * @constructor
 */
export const GetMovieDetails = (id) => Get(`/movie/${id}`)
