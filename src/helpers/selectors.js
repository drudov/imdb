/**
 * Movies Selector
 * @param popular
 * @param genres
 * @returns {*}
 */
export const GetMoviesSelector = ({ popular, genres }) => {
  if (!popular) {
    return false
  }

  return popular.results.map(movie => {
    const gIds = movie.genre_ids

    movie.genres = genres.filter(({ id }) => gIds.includes(id))

    return movie
  })
}

/**
 * Get Total Pages
 * @param popular
 * @returns {*}
 */
export const GetTotalPages = ({ popular }) => popular && popular['total_pages']


/**
 * Get Total Results
 * @param popular
 * @returns {*}
 * @constructor
 */
export const GetTotalResults = ({ popular }) => popular && popular['total_results']

/**
 * Get Current page
 * @param popular
 * @returns {*}
 * @constructor
 */
export const GetCurrentPage = ({ popular }) => popular && popular['page']