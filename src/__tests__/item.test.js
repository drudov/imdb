import React from 'react'
import renderer from 'react-test-renderer'
import ListItem from 'components/moviesList/ListItem'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { List } from 'antd'


const { Item } = List

Enzyme.configure({ adapter: new Adapter() })


it('renders list item', () => {
  const item = {
    id: 1,
    poster_path: 'some_path',
    title: 'title',
    overview: 'overview',
    genres: [{ name: 'genre1' }, { name: 'genre2' }],
    original_title: 'title',
  }

  const listItem = renderer.create(<ListItem {...item}/>).toJSON()
  const rListItem = shallow(<ListItem id={1} {...item}/>)

  expect(rListItem.name()).toEqual('Item')
  expect(rListItem.find('.list-item').length).toEqual(1)
  expect(rListItem.find(Item).length).toEqual(1)

  expect(listItem).toMatchSnapshot()



})