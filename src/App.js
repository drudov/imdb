import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Layout } from 'antd'
import MoviesList from 'components/moviesList/MoviesListContainer'
import routes from 'helpers/url'
import './App.scss'
import MovieContainer from 'components/movie/MovieContainer'

const { Header, Footer, Content, Sider } = Layout


class App extends Component {
  render() {
    return (
      <Router>
        <Layout className="movies">
          <Header/>
          <Content>
            <Switch>
              <Route component={MoviesList} path={routes.moviePageRoute} exact/>
              <Route component={MovieContainer} path={routes.movieDetails()} />
            </Switch>
          </Content>
          <Footer>
            footer
          </Footer>
        </Layout>
      </Router>
    )
  }
}

export default App
