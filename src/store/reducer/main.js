import { createReducer } from 'redux-create-reducer'

export const Main = {
  Update: 'Main.Update',
}

export default createReducer({}, {
  [Main.Update](state, { type, ...data }) {
    return { ...state, ...data }
  },
})