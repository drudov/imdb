import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import main from 'store/reducer/main'

export default combineReducers({
  main,
  form: formReducer,
})