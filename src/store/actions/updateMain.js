import { Main } from 'store/reducer/main'

const updateMain = (data, dispatch) => {
  dispatch({
    type: Main.Update,
    ...data,
  })
}

export default (data) => dispatch => updateMain(data, dispatch)