import { message } from 'antd'
import { getFormValues } from 'redux-form'
import get from 'lodash/get'
import { GetGenres, GetPopular, SearchMovie } from 'helpers/api'
import updateMain from 'store/actions/updateMain'
import { MOVIE_FIELD, SearchFormName } from 'components/searchForm/SearchForm'

/**
 * Get Popular Movies Action
 * @param page
 * @param dispatch
 * @param genres
 * @param store
 * @returns {Promise<boolean>}
 */
const getPopular = async (page, dispatch, { main: { genres }, ...store }) => {
  dispatch(updateMain({ loading: true }))

  const searchForm = getFormValues(SearchFormName)(store)
  let moviesAction = GetPopular(page)
  const movie = get(searchForm, [MOVIE_FIELD], '')

  if (movie.trim().length > 0) {
    moviesAction = SearchMovie(movie, page)
  }

  if (!genres) {
    const gResponse = await GetGenres()
    if (!gResponse) {
      message.error('error getting genres')
      return false
    }
    genres = gResponse.genres
  }

  const popular = await moviesAction

  dispatch(updateMain({ loading: false }))

  if (!popular) {
    message.error('error getting data')
    return false
  }

  dispatch(updateMain({ popular, genres }))

  return true
}

export default (page) => (dispatch, getState) => getPopular(page, dispatch, getState())