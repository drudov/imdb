import { message } from 'antd'
import { GetMovieDetails } from 'helpers/api'
import updateMain from 'store/actions/updateMain'


const getMovieDetails = async (id, dispatch, store) => {
  dispatch(updateMain({ loading: true }))
  const movieDetails = await GetMovieDetails(id)


  if (!movieDetails) {
    message.error('error getting details')
    dispatch(updateMain({ loading: false }))
    return false
  }

  dispatch(updateMain({ loading: false, movieDetails }))

  return true
}


export default (id) => (dispatch, getState) => getMovieDetails(id, dispatch, getState())